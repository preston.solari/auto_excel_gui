# auto_excel_gui

Automated tasks with python/tkinter. The code runs with **Python 3.9.10**.

## Python dependencies

- Pandas
- Numpy
- openpyxl
- Tkinter
- ttkthemes (for GUI theme; optional)
- xlwings
- Matplotlib
- pyinstaller (optional)

## Install/run

To install the program, we simply download the repository:
```sh
$ git clone https://gitlab.com/preston.solari/auto_excel_gui
$ cd auto_excel_gui
```
To run the code, we use:
```sh
$ python3 interface.py
```

It is possible to run this project under a Windows environment having installed the correct dependencies (including pyinstaller) and running:
```sh
$ cd auto_excel_gui
$ pyinstaller --onefile interface.py
```
This generates a **.exe** file in the **dist** folder inside the auto_excel_gui repository.
